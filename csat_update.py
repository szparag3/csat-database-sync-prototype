import win32com.client, csv, datetime, smtplib, time
from selenium import webdriver

try:
    start_time = datetime.datetime.now()
    print(start_time)

    folder = 'H:\\DASHBOARD\\'

    xl = win32com.client.gencache.EnsureDispatch("Excel.Application")
##    xl.Visible = True

    excel_file = folder + 'VSAT.xlsx'

    wb = xl.Workbooks.Open(excel_file)

    print('data update started')
    wb.RefreshAll()
    print('data updated')
    ws = wb.Sheets(1)
    ws.Select

    ws.Columns(1).AutoFilter(1)
    print('colecting data')
    i = 13
    update_needed = []
    while ws.Cells(i,8).Value != None:
        if ws.Cells(i,8).Value == 'Update':
            update_needed.append([ws.Cells(i,5).Value,ws.Cells(i,6).Value])
        i += 1
    dt = datetime.datetime.now()

    output_name = 'auto' + dt.strftime('%Y%m%d%H%M') + '.csv'
    output_name = folder + 'archive\\' + output_name

    print('saving file')

    out = open(output_name, 'w')
    #write header
    out.write('Customer_Feedback__c,')
    out.write('Id')
    out.write('\n')
    row_num = 0
    for row in update_needed:
        row_num += 1
        col_num = 1
        for column in row:
            if col_num > 1:
                out.write(',')
            out.write('{}'.format(column))
            col_num += 1
        out.write('\n')

    #save output
    out.close()
    #save CAST workbook
    wb.Close(True)
    xl.Quit()

    if row_num > 1:

        print('data ready for upload to SFDC')

        print('send csv to dataloader.io')
        browserSfdcUpload = webdriver.Chrome()
        browserSfdcUpload.implicitly_wait(30)
        browserSfdcUpload.get('https://dataloader.io/misc/forms/login/dataloader-login.php?op=tasks#login')
        browserSfdcUpload.find_element_by_id("submit-field").click()
        browserSfdcUpload.find_element_by_id('username').send_keys('XXXXX@XXXX')
        browserSfdcUpload.find_element_by_id('password').send_keys('XXX')
        browserSfdcUpload.find_element_by_id('Login').click()
        browserSfdcUpload.find_element_by_id('cmd-new-task').click()
        browserSfdcUpload.find_element_by_css_selector('a[href="#import"]').click()
        browserSfdcUpload.find_element_by_css_selector('button[data-operation="update"]').click()
        browserSfdcUpload.find_element_by_css_selector('a[data-object="Case"]').click()
        browserSfdcUpload.find_element_by_css_selector('button[class="btn btn-primary btn-large next"]').click()
        browserSfdcUpload.find_element_by_css_selector('input[type="file"]').send_keys(output_name)
        for i in range(20):
            time.sleep(1)
        browserSfdcUpload.find_element_by_css_selector('button[class="btn btn-primary btn-large next"]').click()

        browserSfdcUpload.find_element_by_css_selector('button[class="btn btn-primary btn-large finish-run"]').click()
        browserSfdcUpload.find_element_by_css_selector('button[class="btn ok btn-primary"]').click()
        for i in range(20):
            time.sleep(1)
        browserSfdcUpload.quit()
        emailBody = 'CSAT update completed - {} rows'.format(row_num)
    else:
        emailBody = 'CSAT no data for update'
        print('notification email') 

    end_time = datetime.datetime.now()
    print(end_time)
    delta_time = end_time - start_time
    print("program live time {}".format(delta_time))

    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()
    smtpobj.login('pnawrocki1983@gmail.com', 'XXXXXXX')
    smtpobj.sendmail('pnawrocki1983@gmail.com', 'szparag3@gmail.com', 'Subject: CSAT AUTOMATION\n{}\nprogram live time - {}'.format(emailBody, delta_time))
    smtpobj.quit()

    print('End of Script')
    
except:
    print('HELP -- ERROR ERROR ERROR')
    input()